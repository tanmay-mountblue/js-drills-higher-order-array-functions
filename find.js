module.exports.find = (elements,cb) => {
    let arr=[];
    for (let i = 0; i < elements.length; i++) {
        let res=cb(elements[i]);   
        if(res){
            return elements[i];
        }     
    }
    return undefined;
}