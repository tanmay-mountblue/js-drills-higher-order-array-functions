module.exports.reduce = (elements,cb, startingValue) => {
    let index=startingValue || 0;
    let res=elements[index];
    for (let i = index+1; i < elements.length; i++) {
        res=cb(res,elements[i]);    
    }
    return res;
}